﻿open System // POTRZEBNE DO MATH.PI


//Zad 1
let okwadrat (a:float) = 4.0 * a
let oprostokat (a:float) (b:float) = 2.0*a+2.0*b
let okolo (r:float) = 2.0*Math.PI*r

let pkwadrat (a:float) = a**2.0
let pprostokat (a:float) (b:float) = a*b
let pkolo (r:float) = Math.PI*r**2.0

//Zad 2
let wielomian (x:float) (y:float) = (-3.0*y**4.0) + (0.5*x**2.0) + (2.0*y) - 7.0

//Zad 3
let rec wyrazciagu (a1:int) (q:int) (n:int) =
    if n = 1 then a1
    else q * wyrazciagu (a1) (q) (n-1)

let rec wyrazciagu2 (a1:int) (q:int) (n:int) =
    match n with
    | 1 -> a1
    | _ -> q * wyrazciagu2 (a1) (q) (n-1)

let rec nwd (a:int) (b:int) = 
    if b<>0 then nwd (b) (a%b)
    else a

let rec nwd2 (a:int) (b:int) = 
    match b with
    | b when b<>0 -> nwd2 (b) (a%b)
    | _ -> a

let rec sumacyfr (x:int) =
    if x=0 then 0
    else x%10 + sumacyfr (x/10)

let rec sumacyfr2 (x:int) =
    match x with
    | 0 -> 0
    | _ -> x%10 + sumacyfr2(x/10)


//Zad 4
let rec primeRec (x:int) (div:int) =
    if x = 2 then true
    else if (x < 2 || x%2 = 0) then false
    else if ((float)div**2.0 > (float)x) then true
    else if (x%div=0) then false
    else primeRec (x) (div+2)

let rec primeRec2 (x:int) (div:int) =
    match x with
    | 2 -> true
    | x when (x < 2 || x%2 = 0) -> false
    | x when ((float)div**2.0 > (float)x) -> true
    | x when (x%div=0) -> false
    | _ -> primeRec2 (x) (div + 2)


let pierwsza (x:int) = primeRec (x) (3)
let pierwsza2 (x:int) = primeRec2 (x) (3)
       


//Zad 5
let rec liczby (a:int) (b:int) =
    if (a>=b) then
        printfn "Kolejna liczba %i" a
        liczby (a-1)(b)

let rec wyraz (w:string) (n:int) =
    if (n=1) then w
    else w + wyraz (w) (n-1)
