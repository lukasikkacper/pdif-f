﻿// Learn more about F# at http://fsharp.org

open System


//Zad 1

type RecData = {
    Year : int
    Month : int
    DayOfMonth : int
}

let convertToRecData value = 
    match value with
    | (year,month,dayofmont) -> { Year = year; Month = month; DayOfMonth = dayofmont; }

let recDataToQuote (recdata:RecData) =
    (recdata.Year,recdata.Month,recdata.DayOfMonth)

//Zad 2


let compareDate date1 data2 =
    let d1 = convertToRecData date1
    let d2 = convertToRecData date1
    if date1 = data2 then 0
    else if (d1.Year > d2.Year) || (d1.Year = d2.Year && d1.Month >= d2.Month) || (d1.Year = d2.Year && d1.Month = d2.Month && d1.DayOfMonth > d2.DayOfMonth) then -1
    else 1

//Zad 3

let leapYear y = 
    match y % 4 = 0, y % 400 = 0, y % 100 = 0 with
    | true, true, true -> true
    | true, false, false -> true
    | false, _, _ -> false
    | _ -> false

let dateIsValid (date:RecData) = 
    match date with
    | date when date.Month > 12 || date.Month < 1 -> false
    | date when date.DayOfMonth > 31 -> false
    | date when leapYear date.Year && date.Month = 2 && date.DayOfMonth > 29 -> false
    | date when not <| leapYear date.Year && date.Month = 2 && date.DayOfMonth > 28 -> false
    | date when [4;6;9;11] |> List.contains date.Month && date.DayOfMonth > 30 -> false
    | _ -> true


//Zad4
let readDateFromKeyboard () =
    try
        printfn "Podaj rok: "
        let year = System.Console.ReadLine() |> int

        printfn "Podaj miesiac: "
        let month = System.Console.ReadLine() |> int

        printfn "Podaj dzien: "
        let dayOfMonth = System.Console.ReadLine() |> int

        let date = (year,month,dayOfMonth)

        let validateData = date |> convertToRecData |> dateIsValid

        match validateData with
            | true -> Some (year,month,dayOfMonth)
            | _ -> None
    with
    | exn -> None

//Zad5

type MyEvent = {
    EventName : string;
    Date : RecData;
    Time: int*int;
    Duration: int;
    Place: string;
    Person: string*string
}

let getMinutes time =
    match time with
    | (h,m) -> h*60+m
    

let unifiMyEvents (event1:MyEvent) (event2:MyEvent) =
    match event1 with
    | event1 when event1.Place <> event2.Place -> None
    | event1 when event1.Person <> event2.Person -> None
    | event1 when event1.Date <> event2.Date -> None
    | event1 when (getMinutes event1.Time) + event1.Duration <> getMinutes event2.Time -> None
    | _ -> Some {
        EventName = event1.EventName + " " + event2.EventName;
        Date = event1.Date;
        Time = event1.Time;
        Duration = event1.Duration + event2.Duration;
        Place = event1.Place;
        Person = event1.Person;
    }

readDateFromKeyboard ()

let true1 = { EventName = "test1"; Date = {Year = 2020; Month = 11; DayOfMonth = 25}; Time = (13,00); Duration=60; Place = "Siedlce"; Person = ("Kacper","Łukasik") }
let true2 = { EventName = "test2"; Date = {Year = 2020; Month = 11; DayOfMonth = 25}; Time = (14,00); Duration=60; Place = "Siedlce"; Person = ("Kacper","Łukasik") }

let false1 = { EventName = "test1"; Date = {Year = 2019; Month = 11; DayOfMonth = 25}; Time = (13,00); Duration=60; Place = "Siedlce"; Person = ("Kacper","Łukasik") }
let false2 = { EventName = "test2"; Date = {Year = 2020; Month = 11; DayOfMonth = 25}; Time = (14,00); Duration=60; Place = "Siedlce"; Person = ("Kacper","Łukasik") }

let w1 = unifiMyEvents true1 true2
let w2 = unifiMyEvents false1 false2